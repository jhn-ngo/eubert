
# EuBERT is a POC for improving search across Europeana heritage object using multilingual BERT-based models for NER and embeddings.

## Solution description:

Solution is based on [multilingual BERT-based models](https://github.com/google-research/bert/blob/master/multilingual.md) trained for different purposes:

 - Named Entity Recognition (NER) 
 - Embeddings creation
 - Rearranging results
 
Our solution includes three pipelines of different complexity.
In general our improvements are based on fact that we are working with multilingual embeddings, which allows not to lose information while translation.

Let me describe each pipeline in more details: 

### Pipeline 1:

Simplest pipeline of our solution rearrange results from existing EuropeanaAPI with help of [Nboost](https://github.com/koursaros-ai/nboost) and our pre-trained model.

It includes following steps: 

1. Send source query to the existing EuropeanaAPI in order to get records. 
2. Send results to the Nboost in order to rearrange results based on key (title+entities). 

Scheme of the pipeline is provided below:  

![picture](.github/eubert_pipeline_1.jpg)

### Pipeline 2:

Second pipeline involves new approach: it extract NER's from source query, encode them with pre-trained encoder model.
Then, it search for the closest vectors in pre-defined vector space. 
On the next step it get corresponding records ID's for founded vectors. 
Finally, we merge results achieved from this approach and from EuropeanaAPI and also send them to Nboost for rearrange.

Search flow includes following steps: 

1. Extract NER's via our [NER microservice](src/ner/README.md). If NER is not found in source query go to step 5. 
2. Vectorize NER with pre-trained BERT-based encoder model. 
3. Find closest vectors from entities pre-defined vector space using [Milvus](https://github.com/milvus-io/milvus)
4. Get records ID's of founded vectors by pre-defined key via EuropeanaAPI.
5. Get records of source query using standard EuropeanaAPI (same as step 1 in first pipeline).
6. Merge results from steps 4 and 5. 
7. Send results via Nboost in order to rearrange results based on key (title+entities). 

Scheme of the pipeline is provided below:

![picture](.github/eubert_pipeline_2.jpg)

### Pipeline 3:

Third pipeline is pretty similar to the second, but we encode source query in case there is no NER's founded. 

It includes following steps:

1. Extract NER's via our NER microservice. If NER is not found - encode source query. 
2. Vectorize NER with pre-trained BERT encoder model. 
3. Find closest vectors from entities pre-defined vector space using Milvus.
4. Get records ID's of founded vectors by pre-defined key via EuropeanaAPI.
5. Get records of source query using standard EuropeanaAPI (same as step 1 in first pipeline).
6. Merge results from steps 4 and 5. 
7. Send results via Nboost in order to rearrange results based on key (title+entities). 

Scheme of the pipeline is provided below:

![picture](.github/eubert_pipeline_3.jpg)

## Used models and services

Solution is based on [multilingual BERT-based models](https://github.com/google-research/bert/blob/master/multilingual.md) and use them for different purposes. 

- [NER microservice](src/ner/README.md) - microservice in Docker, which provides API to extract NER's from text. 
- Encoder - is a pre-trained model for creating vector embedding based on BERT.
- [Milvus](https://github.com/milvus-io/milvus) - is vector search engine. 
- [Nboost](https://github.com/koursaros-ai/nboost) - service for rearranging results based on BERT pre-trained model. 


---

## Roadmap:

1. train NER models using Ontonotes-trained multilingual BERT models (cased and uncased),  based on simpletransformers library
2. implement NER microservice based on Flask,
3. fine-tune NER models on Europeana-specific entities (persons, places, concepts, dates)
4. evaluate performance of NER models
5. implement NER-based search (query -> extract NERs -> build NER-based Solr query -> query Europeana API -> return records)
6. implement sentence2vector microservice based on sentence-transformers library, based on both cased and uncased BERT ML models
7. index embeddings of Europeana dataset (~2M records) to Milvus. Additionally, build separate index in Milvus for entities (entities embeddings)
8. implement embeddings search flow (query -> embedding -> search -> closest embeddings -> get records from embeddings IDs (using Europeana API)
9. evaluate embeddings search flow performance
10. combine NER-based search with embeddings search in several possible ways:
    - get records from NER based search, then for each record retrieve several closest records (by embeddings). Merge and range combined results
    - get records from NER based search, then for query embedding retrieve several closest records (by embeddings). Merge and range combined results
    - get records from NER based search, then for entity embedding retrieve several closest records (by entity embeddings). Merge and range combined results

11. develop simple HTML page to compare current Europeana search result with EuBERT results
12. write short report describing architecture, different models and results 
