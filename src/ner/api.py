import os
import argparse
import time
import json
import string
import re
import traceback
import logging
from flask import Flask
from flask_cors import CORS
from flask import request
from simpletransformers.ner import NERModel
from deeppavlov import build_model


# GLOBAL VARIABLES
LOGGING_FILENAME = os.getenv("LOGGING_FILENAME", "/tmp/ner_service.log")


def setup_logger(name, log_file, level=logging.INFO):
    """Function setup as many loggers as you want"""
    formatter = logging.Formatter(
        "%(asctime)s:%(levelname)s:%(message)s", datefmt="%Y-%m-%d %H:%M:%S"
    )
    handler = logging.FileHandler(log_file)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger


GENERAL_LOGGER = setup_logger("general_logger", LOGGING_FILENAME)


# PARSE ARGUMENT
parser = argparse.ArgumentParser(description="Indicate which model to use.")
parser.add_argument(
    "--modeltype",
    type=str,
    help="indicate which model to use: `DP`(DeepPavlov) or `SP` (SimpleTransfomers) ",
)
parser.add_argument(
    "--modelname",
    type=str,
    default="bert",
    help="which model to use: 'bert' (default) or 'distilbert'",
)
parser.add_argument(
    "--modelpath",
    type=str,
    help="full path to model. For example: /app/models/ner_ontonotes_simpletransformers_uncased",
)
args = parser.parse_args()
modeltype = args.modeltype.lower()
modelname = args.modelname
modelpath = args.modelpath


def initialize_model(modeltype, modelname, modelpath):
    if modeltype == "sp":
        labels = [
            l.strip()
            for l in open(os.path.join(modelpath, "labels.txt"), "r").readlines()
            if l.strip()
        ]
        model = NERModel(modelname, modelpath, labels=labels, use_cuda=False,)
    elif modeltype == "dp":
        print("-" * 33)
        print("initializing dp model")
        model = build_model(modelpath, download=False)  # change path here
        print("-" * 33)
        print(f"model {modelpath} initialized")
    else:
        print("Such modeltype is not supported by the service")

    return model


model = initialize_model(modeltype, modelname, modelpath)

# INITIALIZING FLASK
app = Flask(__name__)
CORS(app)


# HELPER FUNCTIONS TO COMBINE NER
def get_word_and_entity(word_dict):
    word = list(word_dict)[0]
    entity = word_dict[word]
    return word, entity


def return_next_entity_index(n, list_):
    next_word_dict = list_[n + 1]
    _, next_entity = get_word_and_entity(next_word_dict)
    if next_entity[0] == "I":
        return "I"
    elif next_entity[0] == "B":
        return "B"


def clean_punctuation(s):
    """function return string s cleaned from punctuation, except `-` sign"""
    str_to_del = string.punctuation.replace("-", "")
    regex = re.compile("[%s]" % re.escape(str_to_del))
    return regex.sub("", s)


def create_preds_mapping(preds_list, text):
    """function create mapping of prediction list to source text"""
    source_map = []
    preds_words_list = [list(i)[0] for i in preds_list]
    for m in re.finditer(r"(\w+|\S)", text):
        if m[0] in preds_words_list:
            index = preds_words_list.index(m[0])
            source_map.append([preds_words_list[index], m.span()])
            preds_words_list.pop(index)
    return source_map


# GET MODEL PREDICTIONS
def get_model_results(text):
    if modeltype == "dp":
        name_of_model = f"{modelname}-deeppavlov"
    elif modeltype == "sp":
        name_of_model = f"{modelname}-{modelpath.split('_')[-1]}"
    template = {
        "model": f"{name_of_model}",
        "entities": [],
        "ok": False,
    }
    if modeltype == "dp":
        preds_list_dp = model([text])
        preds_list = list()
        for word, ent in zip(preds_list_dp[0][0], preds_list_dp[1][0]):
            if ent != 0:
                preds_list.append({word: ent})
        print(f"preds_list: {preds_list}")
    elif modeltype == "sp":
        preds_list, preds_arr = model.predict([text])
        preds_list = preds_list[0]  # because bert return list of list of dictionaries

    source_map = create_preds_mapping(preds_list, text)  # to restore correct indeces
    l = len(preds_list)
    for n, word_dict in enumerate(preds_list):
        word, entity = get_word_and_entity(word_dict)
        span = [i for i in source_map if i[0] == word][0][
            1
        ]  # getting source span from source_map
        start = span[0]  # get start position
        index = source_map.index([word, span])  # found index of element
        source_map.pop(index)  # remove founded element

        if entity == "O":
            continue

        whitespace_n = 0  # to count number of added whitespaces
        while n < (l - 1) and return_next_entity_index(n, preds_list) == "I":
            n = n + 1
            word_new, entity_new = get_word_and_entity(preds_list[n])
            word = word + " " + word_new
            whitespace_n += 1
            preds_list[n][word_new] = "skip"

        if entity == "skip":
            continue

        entity = entity.split("-")[1]
        if whitespace_n>0:
            whitespace_n = whitespace_n - 1
        end = start + len(word) - whitespace_n
        ner = {
            "value": clean_punctuation(word),
            "entity": entity,
            "start": start,
            "end": end,
        }
        template["entities"].append(ner)

    template["ok"] = True
    print(template)
    return template


@app.route("/")
def index():
    return "Welcome to the API for NER extraction based on multilingual BERT!"


@app.route("/ner", methods=["POST", "OPTION"])
def get_ners():

    ner = []
    elapsed_time = 0
    is_input_correct = False

    try:
        start = time.time()
        data = request.get_json()
        text = data.get("text")

        if len(text) == 0:
            GENERAL_LOGGER.error(
                "No text were send to the ner service:\n" + traceback.format_exc()
            )

        ner = get_model_results(text)
        is_input_correct = True
        elapsed_time = round((time.time() - start), 3)
    except MemoryError:
        GENERAL_LOGGER.error("There is no enough memory:\n" + traceback.format_exc())
    except:
        GENERAL_LOGGER.error(
            "Undefined error in NER service:\n" + traceback.format_exc()
        )
    finally:
        result = {
            "source_text": text,
            "response": ner,
            "time": elapsed_time,
            "is_input_correct": is_input_correct,
        }
        return json.dumps(result)


if __name__ == "__main__":
    from api import app

    app.run(host="0.0.0.0", port=6060)
