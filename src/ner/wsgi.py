#!/usr/bin/python
from api import app
from api import Flask
import argparse


def create_app(modelname, modelpath):
    app = Flask(__name__)
    app.config["modeltype"] = modeltype
    app.config["modelname"] = modelname
    app.config["modelpath"] = modelpath
    print("Passed modeltype: ", app.config["modeltype"])
    print("Passed modelname: ", app.config["modelname"])
    print("Passed modelpath: ", app.config["modelpath"])
    return app


if __name__ == "__main__":
    # PARSE ARGUMENT
    parser = argparse.ArgumentParser(description="Indicate which model to use.")
    parser.add_argument(
        "--modeltype", type=str, help="indicate which model to use: `DP`(DeepPavlov) or `SP` (SimpleTransfomers) ",
    )
    parser.add_argument(
        "--modelname",
        type=str,
        default="bert",
        help="which model to use: 'bert' (default) or 'distilbert'",
    )
    parser.add_argument(
        "--modelpath", type=str, help="path to model",
    )
    args = parser.parse_args()
    modeltype = args.modeltype.lower()
    modelname = args.modelname
    modelpath = args.modelpath
    create_app(modelname, modelpath)
    app.run(host="0.0.0.0", port=6060)
