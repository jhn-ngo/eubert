# NER SERVICE

API Service for NER extraction from raw text. 

## How to interact with API?

All interaction works by HTTP POST requests.

You need to send JSON to service on URL of the service /ner. <br>

For example if service hosted on the localhost: http://localhost:8000/ner

### Structure of JSON:

**Input:**

```json
{
    "text": "YOUR_TEXT"
}
```

**Output:**

```json
{
    "source_text": "YOUR_TEXT",
    "response": {
        "model": "MODELNAME",
        "entities": [
            {
                "value_1": "VALUE_1",
                "entity_1": "NER_1",
                "start_1": "START_1",
                "end_1": "END_1"
            },
            {
                "value_n": "VALUE_n",
                "entity_n": "NER_n",
                "start_n": "START_n",
                "end_n": "END_n"
            }
        ],
        "ok": "BOOLEAN"
    },
    "time": "TIME",
    "is_input_correct": "BOOLEAN"
}
```

Meaning of parameters:
* source_text - input text for the service
* response - contain result with predicted ner
  * model - indicates which model was used to make predictions (string)
  * entities - contain array of detected NER (array of dictionaries)
    * value_n - word of n-th predicted entity (string)
    * entity_n - entity of n-th predicted NER (string)
    * start_n - start index of n-th predicted word
    * end_n - end index of n-th predicted word
  * ok - flag shows that model works (boolean)
* time - contain time (in seconds) spent for identity NER by model (float)
* is_input_correct - flag if input was correct (boolean)

### Example: 

**Request:**

```json
{
	"text": "I would like to visit moscow. Museum of art in Tel aviv, yaffo."
}

```

**Curl format request:**

```
curl --location --request POST '209.182.238.238:7678/ner' \
--header 'Content-Type: application/json' \
--data-raw '{
	"text": "Tel aviv is a best place to visit"
}
'
```

**Response:**

```json
{
    "source_text": "I would like to visit Moscow. Museum of art in Tel aviv, yaffo.",
    "response": {
        "model": "bert-uncased",
        "entities": [
            {
                "value": "Moscow",
                "entity": "GPE",
                "start": 22,
                "end": 28
            },
            {
                "value": "Museum of art",
                "entity": "ORG",
                "start": 30,
                "end": 43
            },
            {
                "value": "Tel aviv",
                "entity": "GPE",
                "start": 47,
                "end": 55
            },
            {
                "value": "yaffo",
                "entity": "GPE",
                "start": 57,
                "end": 62
            }
        ],
        "ok": true
    },
    "time": 0.89,
    "is_input_correct": true
}
```


## To build locally: 

Go to the directory with DockerFile:

```bash
cd src/ner
```

Then build docker image:

_Note:_ Make sure you have rights to run docker processes or use sudo. 

```bash
docker build -t ner_service .
```

Finally, run docker container: 

Don't forget to indicate parameters: 
 - `modeltype`:  `dp` (DeepPavlov) or `sp` (SimpleTransformers)
 - `modelname`: is name for model to use: `bert` (default) or `distilbert` 
 - `modelpath`: path to the models in the container.

In general looks like that: 

```bash
docker run -it - d --rm --name ner_service -p 8000:6060 -v PATH_TO_MODELS:/app/models  ner_service --modelname MODELNAME --modelpath MODELPATH
```

Example for DeepPavlov:
```bash
sudo docker run -it -d --rm --name ner_service -p 7678:6060 -v /opt/eubert/models/ontonotes_mixed/:/app/models/ontonotes_mixed/ -v /opt/models/deeppavlov/downloads/bert_models/multi_cased_L-12_H-768_A-12/:/app/downloads/bert_models/multi_cased_L-12_H-768_A-12/ -v /opt/models/deeppavlov/ner_ontonotes_bert_mult_mixed/:/app/models/ner_ontonotes_bert_mult_mixed/ ner_service:latest --modeltype dp --modelname bert --modelpath /app/dp_settings.json
```

Example for SimpleTransformers: 
```bash
sudo docker run -it -d --rm --name ner_service -p 7678:6060 -v /opt/eubert/models:/app/models  ner_service --modeltype sp --modelname bert  --modelpath /app/models/ner_ontonotes_simpletransformers_uncased
```

After that you could go to http://localhost:8000/ to check if API is working.
