### Fixes to NBOOST

Nboost could be installed from here: https://pypi.org/project/nboost/#description

We also planned to get the latest version from the for of the repo and install it from there. 

Right now just changed what already have in two files: 
`cli.py` and `proxy.py`

Now it supports `config_file` parameter while running which except to get path to CONFIG_MAP.json like in this folder. 

Don't forget to activate venv which contains nboost, for instance:

```bash
cd /opt/eubert
source venv/bin/activate
```

Full command example in current version:
 
```bash
nboost --data_dir /opt/eubert/models --model_dir bert_multilingual_cased_msmarco --uport 5757 --config_file /opt/eubert/src/nboost/CONFIG_MAP.json --config eubert_key --verbose VERBOSE
``` 