You need to rename .env.example to .env and update the environment variables pointing to both API's.
To build for production do the following:

`$ npm install`
`$ npm run build`

p.s. The _package.json's_ **postbuild** script will place the production ready build to _../search/templates_ directory so it can be served with the search microservice.
