import React from 'react';
import PipelineSwitcher from '../PipelineSwitcher/PipelineSwitcher';
import SearchInput from '../SearchInput/SearchInput';
import classes from './Options.module.css';

const pipelineChangeAllowed =
  !!process.env.REACT_APP_PIPELINE_CHANGE_ALLOWED | false;

const Options = props => {
  return (
    <div className={classes.Options}>
      <div className={classes.inputContainer}>
        <SearchInput
          current={props.current}
          onChange={props.onChange}
          onSubmit={props.onSubmit}
        />
      </div>
      {pipelineChangeAllowed ? (
        <div className={classes.pipelineSwitcherContainer}>
          (
          <PipelineSwitcher
            pipelines={props.pipelines}
            onPipelineChange={props.onPipelineChange}
          />
          )
        </div>
      ) : null}
    </div>
  );
};

export default Options;
