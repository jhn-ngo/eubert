import React from 'react';
import Search from '../Search/Search';
import classes from './SearchView.module.css';

const {
  REACT_APP_SEARCH1_APP,
  REACT_APP_SEARCH1_API_URL,
  REACT_APP_SEARCH1_USE_NER,
  REACT_APP_SEARCH2_APP,
  REACT_APP_SEARCH2_API_URL,
  REACT_APP_SEARCH2_USE_NER,
} = process.env;

const SearchView = props => {
  return (
    <div className={classes.container}>
      {REACT_APP_SEARCH1_API_URL ? (
        <Search
          id="1"
          searchValue={props.submitted}
          pipeline={props.submitted}
          opts={{
            base: {
              app: REACT_APP_SEARCH1_APP,
              url: REACT_APP_SEARCH1_API_URL,
            },
            list: {
              size: props.searchQuerySize,
            },
          }}
          useNer={!!REACT_APP_SEARCH1_USE_NER}
        />
      ) : null}
      {REACT_APP_SEARCH2_API_URL ? (
        <Search
          id="2"
          searchValue={props.submitted}
          pipeline={props.pipeline}
          opts={{
            base: {
              app: REACT_APP_SEARCH2_APP,
              url: REACT_APP_SEARCH2_API_URL,
            },
            list: {
              size: props.searchQuerySize,
            },
          }}
          useNer={!!REACT_APP_SEARCH2_USE_NER}
        />
      ) : null}
    </div>
  );
};

export default SearchView;
