import { SingleDropdownRange } from '@appbaseio/reactivesearch';
import React from 'react';

const PipelineSwitcher = props => {
  return (
    <SingleDropdownRange
      componentId="dropdown"
      dataField="dummy"
      placeholder="Pipeline"
      data={props.pipelines.options}
      onChange={pipeline => {
        props.onPipelineChange(pipeline.id);
      }}
      value={props.pipelines.active ? props.pipelines.active.label : ''}
    />
  );
};

export default PipelineSwitcher;
