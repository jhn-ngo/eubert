import { DataSearch } from '@appbaseio/reactivesearch';
import React from 'react';

const SearchInput = props => {
  return (
    <DataSearch
      autoFocus={true}
      componentId="searchbox"
      dataField="dummy"
      autosuggest={false}
      placeholder="Search"
      value={props.current}
      onChange={current => props.onChange(current)}
      onKeyDown={event => {
        if (event.key === 'Enter') {
          props.onSubmit();
        }
      }}
    />
  );
};

export default SearchInput;
