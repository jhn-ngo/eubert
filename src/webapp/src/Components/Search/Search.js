import {
  DataSearch,
  ReactiveBase,
  ReactiveList,
} from '@appbaseio/reactivesearch';
import React from 'react';
import { ItemContext } from '../../App';
import Item from '../Item/Item';
import classes from './Search.module.css';

const Search = props => {
  const searchId = `search_${props.id}`;
  const dropdownId = `dropdown_${props.id}`;
  const listId = `list_${props.id}`;
  return (
    <div className={classes.container}>
      <ReactiveBase app={props.opts.base.app} url={props.opts.base.url}>
        <DataSearch
          className={classes.hidden}
          componentId={searchId}
          dataField="dummy"
          autosuggest={false}
          placeholder="Value"
          value={props.searchValue}
        />
        <DataSearch
          className={classes.hidden}
          componentId={dropdownId}
          dataField="dummy"
          autosuggest={false}
          placeholder="Pipeline"
          value={props.pipeline}
        />
        <ReactiveList
          loader="Loading..."
          componentId={listId}
          dataField="dummy"
          size={props.opts.list.size}
          pagination={true}
          react={{
            and: [searchId, dropdownId],
          }}
          render={({ data }) => (
            <ReactiveList.ResultListWrapper>
              {data.map((item, index) => (
                <ItemContext.Consumer key={index}>
                  {context => (
                    <Item
                      item={item}
                      id={`${searchId}_${dropdownId}_${listId}_${index}_${props.pipeline}`}
                      onClick={context.openItemPage}
                      updateNerData={
                        props.useNer ? context.updateNerData : null
                      }
                    />
                  )}
                </ItemContext.Consumer>
              ))}
            </ReactiveList.ResultListWrapper>
          )}
        />
      </ReactiveBase>
    </div>
  );
};

export default Search;
