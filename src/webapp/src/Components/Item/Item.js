import { ResultList } from '@appbaseio/reactivesearch';
import React, { Component } from 'react';
import classes from './Item.module.css';

class Item extends Component {
  componentDidUpdate(prevProps, prevState) {
    if (this.props.updateNerData) {
      this.props.updateNerData(this.props.item.title[0], `#${this.props.id}`);
    }
  }

  render() {
    const { item } = this.props;

    return (
      <ResultList className={classes.ResultList}>
        <ResultList.Image
          src={
            item.edmPreview && item.edmPreview.length > 0
              ? item.edmPreview[0]
              : null
          }
        />

        <ResultList.Content>
          <ResultList.Title
            className={classes.ItemTitle}
            onClick={() => {
              this.props.onClick(item.guid);
            }}
          >
            <div id={`${this.props.id}`} title="Click to see more">
              {item.title[0]}
            </div>
          </ResultList.Title>
          <ResultList.Description>
            <div>
              <strong>{item.year}</strong>
            </div>
            <div>
              {item.dcDescription ? (
                <div>{item.dcDescription.join(' ')}</div>
              ) : null}
            </div>
            <div>{item.dcCreator}</div>
          </ResultList.Description>
        </ResultList.Content>
      </ResultList>
    );
  }
}

export default Item;
