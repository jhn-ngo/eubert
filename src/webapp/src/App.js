import { ReactiveBase } from '@appbaseio/reactivesearch';
import axios from 'axios';
import React, { Component } from 'react';
import Layout from './Components/hoc/Layout/Layout';
import Options from './Components/Options/Options';
import SearchView from './Components/SearchView/SearchView';
import './index.css';
import displaCyENT from './lib/displacy-ent';

const { REACT_APP_PIPELINE_URL, REACT_APP_NER_API_URL } = process.env;

const searchQuerySize = 10;
let displacy;

export const ItemContext = React.createContext();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: '',
      submitted: '',
      pipelines: {
        active: null,
        options: [
          { id: 1, label: 'Europeana + nboost' },
          { id: 2, label: 'Europeana + NER + nboost' },
          { id: 3, label: 'Europeana + NER/query + nboost' },
        ],
      },
    };
  }

  componentDidMount = () => {
    if (!!REACT_APP_PIPELINE_URL) {
      this.fetchActivePipeline();
    }
    displacy = new displaCyENT('dummy', {});
  };

  fetchActivePipeline = () => {
    axios.get(REACT_APP_PIPELINE_URL).then(
      response => {
        this.updateActivePipelineState(response.data.Pipeline);
      },
      reason => console.error(reason)
    );
  };

  postActivePipeline = id => {
    axios.post(REACT_APP_PIPELINE_URL, { baseline: id }).then(
      response => {
        if (response.status === 200) {
          this.updateActivePipelineState(response.data.Pipeline);
        }
      },
      reason => console.error(reason)
    );
  };

  updateActivePipelineState = id => {
    this.setState(prev => {
      prev.pipelines.active = prev.pipelines.options.filter(
        next => next.id === id
      )[0];
      return prev;
    });
  };

  onInputChangeHandler = current => {
    this.setState(() => ({ current }));
  };

  onSubmitHandler = () => {
    this.setState(
      prevState => ({
        submitted: prevState.current,
      }),
      () => {
        this.requestAndRenderNerVisualData(this.state.submitted, '#search');
      }
    );
  };

  onOpenItemPage = url => {
    window.open(url, '_blank');
  };

  requestAndRenderNerVisualData = (text, selector) => {
    axios.post(REACT_APP_NER_API_URL, { text }).then(
      response => {
        if (response.data.response.entities) {
          displacy.render(
            response.data.source_text,
            response.data.response.entities.map(next => ({
              end: next.end,
              start: next.start,
              type: next.entity,
            })),
            response.data.response.entities.map(next =>
              next.entity.toLowerCase()
            ),
            selector
          );
        }
      },
      reason => console.error(reason)
    );
  };

  render() {
    return (
      <Layout>
        <ReactiveBase app="app" url="http://dummy">
          <Options
            current={this.state.current}
            onChange={this.onInputChangeHandler}
            onSubmit={this.onSubmitHandler}
            pipelines={this.state.pipelines}
            onPipelineChange={this.postActivePipeline}
          />
          <div id="search" className="entities"></div>
          {this.state.submitted ? (
            <ItemContext.Provider
              value={{
                openItemPage: this.onOpenItemPage,
                updateNerData: this.requestAndRenderNerVisualData,
              }}
            >
              <SearchView
                submitted={this.state.submitted}
                pipeline={
                  this.state.pipelines.active
                    ? this.state.pipelines.active.label
                    : ''
                }
                searchQuerySize={searchQuerySize}
              />
            </ItemContext.Provider>
          ) : null}
        </ReactiveBase>
      </Layout>
    );
  }
}

export default App;
