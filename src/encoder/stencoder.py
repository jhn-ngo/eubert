import numpy
from sentence_transformers import SentenceTransformer


class STEncoder:
    def __init__(self, model_or_path):
      self.model = SentenceTransformer(model_or_path)

    
    def encode(self, text_list):
      return self.model.encode(text_list)


if __name__ == "__main__":
    import sys, tqdm, csv
    encoder = STEncoder(sys.argv[1])
    fname = sys.argv[2]
    suffix = ""
    if len(sys.argv) > 3:
       suffix = sys.argv[3]
    print("SUFFIX:{}".format(suffix))   

    BATCH = 50
    text_list = []
    narrays = []
    nkeys = []
    fembed= open("embeddings.bin{}".format(suffix), "wb")
    fkeys = open("keys.txt{}".format(suffix), "w")

    with open(fname, "r") as f:
      fcsv = csv.reader(f)
      for row in tqdm.tqdm(fcsv):
#        key,title_with_entities = row[0], ",".join(row[1:])
#        text_list.append(title_with_entities)
        if "titles" in fname:
          key,title_with_entities = row[0], ",".join(row[1:])
          text_list.append(title_with_entities)
        elif "entities" in fname:
          key, entity = row[1], row[0]
          text_list.append(entity)
        nkeys.append(key)
#        print(len(text_list)) 
        if len(text_list) >= BATCH:
           narrays = encoder.encode(text_list)
           for narray in narrays: numpy.save(fembed,narray)
           for key in nkeys: fkeys.write(key + '\n')
           text_list = []
           nkeys = []
      
      if text_list:
         narrays += encoder.encode(text_list)
         for narray in narrays: numpy.save(fembed,narray)
         for key in nkeys: fkeys.write(key + '\n')
         
