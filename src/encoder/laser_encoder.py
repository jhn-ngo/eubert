""" Sentence encoder script

This script allows to encode sentences with help of LaserEmbeddings:
https://github.com/yannvgn/laserembeddings
This tool accepts full entities filename and suffix, which set up naming of created files.
It generates two files: .bin file containing embeddings and .txt file containing keys

This script requires that `numpy`, `tqdm` and `laserembeddings` be installed within the Python
environment you are running this script in.

This file cannot be imported as a module.
"""


import numpy
import argparse, tqdm, csv
from laserembeddings import Laser


def set_parser() -> argparse.ArgumentParser:
    """Setup parser needed for the script. No any parameters required.

    Returns
    -------
    parser
        an ArgumentParser object, from which entities filename and suffix could be extracted
    """

    parser = argparse.ArgumentParser(description="Parse encoder params: ")
    parser.add_argument("entities_filename", metavar="F", help="path and filename of entities")
    parser.add_argument("suffix", metavar="S", help="suffix to add in generated files")

    return parser


if __name__ == "__main__":
    # Parse params from CLI:
    parser = set_parser()
    args = parser.parse_args()
    fname = args.entities_filename
    suffix = args.suffix
    print(f"Parsed params: \n entities filename: {fname} \n suffix: {suffix}")

    # Setup variables
    BATCH = 50
    text_list = []
    narrays = []
    nkeys = []
    fembed= open(f"embeddings_{suffix}.bin", "wb")
    fkeys = open(f"keys_{suffix}.txt", "w")

    # Initializing LASER
    print("Initializing Laser ...")
    laser = Laser()

    # main loop for reading csv and writing embeddings and keys
    with open(fname, "r") as f:
        fcsv = csv.reader(f)
        for row in tqdm.tqdm(fcsv):
            if "titles" in fname:
                key, title_with_entities = row[0], ",".join(row[1:])
                text_list.append(title_with_entities)
            elif "entities" in fname:
                key, entity = row[1], row[0]
                text_list.append(entity)
            nkeys.append(key)
            if len(text_list) >= BATCH:
                #print(f"text_list: {text_list}")
                narrays = [i for i in laser.embed_sentences(text_list, lang='en').tolist()]
                #print(narrays)
#                print(f"narrays type: {type(narrays)} ... len: {len(narrays)}, first el: {narrays[0].shape} ..  {type(narrays[0])}")
                for narray in narrays:
#                    print(f"narray: {narray}, type: {type(narray)}, shape: {narray.shape}")
                    numpy.save(fembed, narray)
                for key in nkeys: fkeys.write(key + '\n')
                text_list = []
                nkeys = []

        if text_list:
            narrays += [i for i in laser.embed_sentences(text_list, lang='en').tolist()]
            for narray in narrays: numpy.save(fembed, narray)
            for key in nkeys: fkeys.write(key+ '\n')
