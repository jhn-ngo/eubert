## SCRIPTS FOR XML PARSING

**PARSE FROM SINGLE XML**

```bash
python parse_one_xml.py --filename FILENAME
```

will print entities which parsed from that file to terminal.

Example:

```
File: 1AAAE9A1B793D4271C47196C60C73B72CE30E620_test.rdf parsed for 0.018 seconds. It contains 25 entites.
---------------------------------
PERSONS:  ['Léonard Foujita', 'Louis Aragon', 'Pierre Albert Birot', 'Pierre Bertin', 'Цадкин, Осип', 'Marcel Duchamp', 'Луі Арагон', 'Orloff, Chana', 'Jean Cocteau', 'Aragon, Louis', 'Edith Stockhausen', 'Fernand Ledoux', 'Jean Marie Drot', 'Sylvia Beach', 'Chana Orloff', 'Cocteau, Jean', 'Lumia Czekowska', 'Mane-Katz', 'Ossip Zadkine']
PLACES:  ['Paris']
CONCEPTS:  ['Série', 'वृत्त चित्र', 'Documentaire']
DATES:  ['1989-09-20']
TITLES:  ['Petite chronique du Montparnasse pendant la guerre 14-18']
```


**PARSE FROM ZIP**


```bash
 python parse_zip.py --zipname zipname  --bert bert --ent_file ent_file --title_file titles
```

where: 

- `zipname` - full path to zip (example: `/tmp/test_new.zip`)
- `bert` - indicate if we want to separate first and next tokens (example: `true`)
- `ent_file` - name of output file for entities without ext (example: `entities`)
- `title_file` - name of output file for titles without ext (example: `titles`)

Full example: 
```bash
  python parse_zip.py --zipname /tmp/test_new.zip  --bert true --ent_file entities --title_file titles
```

will create two files in current directory: 

`entities.txt` and `titles.txt`

`entities.txt` looks like: 

```
Paris	GPE

Dokumentarni film	CONCEPT

Documentaire	CONCEPT

Série	CONCEPT

1989-09-20	DATE

Agricultura	CONCEPT

Première Guerre mondiale	CONCEPT
```

while `titles.txt` looks like: 

```bash
1/1AAAE9A1B793D4271C47196C60C73B72CE30E620_test.rdf, Petite chronique du Montparnasse pendant la guerre 14-18, Jean Cocteau; Cocteau, Jean; Orloff, Chana; Chana Orloff; Mane-Katz; Marcel Duchamp; Цадкин, Осип; Луі Арагон; Aragon, Louis; Edith Stockhausen; Pierre Albert Birot; Lumia Czekowska; Marcel Duchamp; Jean Marie Drot; Mane-Katz; Fernand Ledoux; Léonard Foujita; Jean Cocteau; Pierre Bertin; Ossip Zadkine; Louis Aragon; Chana Orloff; Sylvia Beach; Paris; Dokumentarni film; Documentaire; Série; 1989-09-20
```

Output of script should look like: 

```bash
WARNING:root:Parsing ../../../tmp/test_new.zip
WARNING:root:Parsed 0 segments
WARNING:root:Parsed 0 segments
---------------------------------
Parsed 37 entities in 0.01 seconds from 2 files.
Entities written to file: entities.txt
Titles written to file: titles.csv
```