import os
import re
import time
import random
import string
import zipfile
import argparse
import requests
import json
import csv
import tqdm
from lxml import etree  # need this to support `tag`


NS_EDM_AGENT = "{http://www.europeana.eu/schemas/edm/}Agent"
NS_SKOS_CONCEPT = "{http://www.w3.org/2004/02/skos/core#}Concept"
NS_ORE_PROXY = "{http://www.openarchives.org/ore/terms/}Proxy"
XML_LANG = "{http://www.w3.org/XML/1998/namespace}lang"

tag_prefLabel = "{http://www.w3.org/2004/02/skos/core#}prefLabel"
tag_altLabel = "{http://www.w3.org/2004/02/skos/core#}altLabel"

EUROPEANA_PROXY = "{http://www.europeana.eu/schemas/edm/}europeanaProxy"
tag_dc_contributor = "{http://purl.org/dc/elements/1.1/}contributor"
tag_dc_spatial = "{http://purl.org/dc/terms/}spatial"
tag_dc_issued = "{http://purl.org/dc/terms/}issued"
tag_dc_type = "{http://purl.org/dc/elements/1.1/}type"
tag_dc_title = "{http://purl.org/dc/elements/1.1/}title"
tag_dc_subject = "{http://purl.org/dc/elements/1.1/}subject"

flatten = lambda l: [item for sublist in l for item in sublist]


def clean_punctuation(s):
    """function return string s cleaned from punctuation"""
    str_to_del = string.punctuation.replace("-", "")
    regex = re.compile("[%s]" % re.escape(str_to_del))
    return regex.sub("", s)


def randomly_select_entry(list_of_entries):
    """Function randomly select entry which is not in language which is forbidden.
    It return dictionary with format {lang:value}, where lang and value are strings.
    If there is no such entries it returns None.

    List of forbidden languages:
    'ko': '장 콕토'
    'zh': '让·谷克多'
    'ja': 'ジャン・コクトー'
    'ka': 'ჟან კოქტო'
    'hy': 'Ժան Կոկտո'
    'ar': 'جان كوكتو'
    'hi': 'प्रथम विश्वयुद्ध'
    'yi': 'דער ערשטער וו'
    """

    languages_not_use = ["ko", "zh", "ja", "ka", "hy", "ar", "hi", "yi"]

    list_of_langs_in_dictionary = [list(entry)[0] for entry in list_of_entries]
    list_of_allowed_langs = list(
        set(list_of_langs_in_dictionary).difference(languages_not_use)
    )
    list_of_entries = [
        dict_a for dict_a in list_of_entries if list(dict_a)[0] in list_of_allowed_langs
    ]

    if len(list_of_entries) > 0:
        lang_val_dict = random.choice(list_of_entries)
        entity = lang_val_dict[list(lang_val_dict)[0]]
    else:
        entity = ""

    return entity


def parse_ore_proxy(segment):
    (
        persons_entity,
        locations_entity,
        dates_entity,
        concepts_entity,
        subject_entity,
        titles_encoding,
    ) = ([], [], [], [], [], [])

    if len(segment.findall(EUROPEANA_PROXY)) > 0:

        is_europeana_proxy = segment.findall(EUROPEANA_PROXY)[0].text
        if is_europeana_proxy == "false":  # check that it's no europeana proxy

            persons_entity = [
                a.text
                for a in segment.findall(tag_dc_contributor)
                if a.text not in ["", None]
            ]
            locations_entity = [
                a.text
                for a in segment.findall(tag_dc_spatial)
                if a.text not in ["", None]
            ]
            dates_entity = [
                a.text
                for a in segment.findall(tag_dc_issued)
                if a.text not in ["", None]
            ]
            concepts_entity = [
                a.text for a in segment.findall(tag_dc_type) if a.text not in ["", None]
            ]
            subject_entity = [
                a.text
                for a in segment.findall(tag_dc_subject)
                if a.text not in ["", None]
            ]
            titles_encoding = [
                a.text
                for a in segment.findall(tag_dc_title)
                if a.text not in ["", None]
            ]

    return (
        persons_entity,
        locations_entity,
        dates_entity,
        concepts_entity,
        subject_entity,
        titles_encoding,
    )


def get_random_entity_from_segment_by_tag(segment, tag):
    entity = ""
    labels = segment.findall(tag)
    if len(labels) > 0:
        entities = [
            {element.attrib[XML_LANG]: element.text}
            for element in labels
            if len(element.attrib) > 0 and element.text not in ["", None]
        ]
        if len(entities) > 0 and entities is not None:
            entity = randomly_select_entry(entities)
    return entity


def parse_skos_concept(segment):
    concept = get_random_entity_from_segment_by_tag(segment, tag_prefLabel)
    return [concept]


def parse_edm_agent(segment):
    persons = list()
    pref_entity = get_random_entity_from_segment_by_tag(segment, tag_prefLabel)
    persons.append(pref_entity)
    alt_entity = get_random_entity_from_segment_by_tag(segment, tag_altLabel)
    persons.append(alt_entity)
    persons = [i for i in persons if i != ""]
    return persons


def parse_zip(path_to_zip):
    print(f"Parsing {path_to_zip}")
    fname, fext = os.path.splitext(path_to_zip)
    if fext == ".zip":
        zip = zipfile.ZipFile(path_to_zip, "r")
        tmx_fnames = zip.namelist()
    else:
        zip = None
        tmx_fnames = [path_to_zip]

    for tmx_fname in tqdm.tqdm(tmx_fnames, desc="XML records"):
        tmx_fname_base = os.path.basename(tmx_fname)  #
        if tmx_fname_base:  # check if this is not a directory
            tmx_file = zip.open(tmx_fname) if zip else open(tmx_fname, mode="rb")
            context = etree.iterparse(
                tmx_file,
                events=("start",),
                tag=[NS_EDM_AGENT, NS_ORE_PROXY, NS_SKOS_CONCEPT],
            )
            try:
                i = 0
                for segment in iter(
                    context
                ):  # If found any invalid part in xml, stop the process
                    # if i % 100 == 0:
                    #      print(f"Parsed {i} segments")
                    #      print(f"Sample segment: {segment}")
                    i += 1
                    yield segment, tmx_fname
            except etree.XMLSyntaxError:  # check if file is well formed
                print(f"Skipping invalid XML {tmx_fname}")


def extract_entities_from_zip(path_to_zip, n_need_ent):
    start = time.time()

    filenames = list()
    persons, places, concepts, dates, titles = (list(), list(), list(), list(), list())
    n_parsed_ent = 0
    n_files = 0

    for segment, filename in parse_zip(path_to_zip):
        if filename not in filenames:
            if n_files != 0:  # skip first
                persons.append(flatten(persons_l))
                places.append(flatten(places_l))
                concepts.append(flatten(concepts_l))
                dates.append(flatten(dates_l))
                titles.append(flatten(titles_l))
            n_files += 1
            filenames.append(filename)

            persons_l, places_l, concepts_l, dates_l, titles_l = (
                list(),
                list(),
                list(),
                list(),
                list(),
            )

        if n_parsed_ent > n_need_ent and n_need_ent != 0:
            break

        edm_persons = list()
        (
            ore_persons,
            ore_locations,
            ore_dates,
            ore_concepts,
            ore_subjects,
            ore_titles,
        ) = (
            list(),
            list(),
            list(),
            list(),
            list(),
            list(),
        )
        skos_concept = list()
        if segment[1].tag == NS_EDM_AGENT:
            edm_persons = parse_edm_agent(segment[1])
            n_parsed_ent += len(edm_persons)
        elif segment[1].tag == NS_ORE_PROXY:
            (
                ore_persons,
                ore_locations,
                ore_dates,
                ore_concepts,
                ore_subjects,
                ore_titles,
            ) = parse_ore_proxy(segment[1])
            n_parsed_ent += (
                len(ore_persons)
                + len(ore_locations)
                + len(ore_dates)
                + len(ore_concepts)
                + len(ore_subjects)
                + len(ore_titles)
            )
        elif segment[1].tag == NS_SKOS_CONCEPT:
            skos_concept = parse_skos_concept(segment[1])
            n_parsed_ent += len(skos_concept)

        persons_l.append(list(set(edm_persons + ore_persons)))
        places_l.append(list(set(ore_locations)))
        concepts_l.append(list(set(ore_concepts + skos_concept + ore_subjects)))
        dates_l.append(list(set(ore_dates)))
        titles_l.append(list(set(ore_titles)))

    # ADD ENTITIES FROM LAST PARSED FILE
    persons.append(flatten(persons_l))
    places.append(flatten(places_l))
    concepts.append(flatten(concepts_l))
    dates.append(flatten(dates_l))
    concepts.append(flatten(concepts_l))
    titles.append(flatten(titles_l))

    entities = {
        "filenames": filenames,
        "persons": persons,
        "places": places,
        "concepts": concepts,
        "dates": dates,
        "titles": titles,
    }

    print("-" * 33)
    print(
        f"Parsed {n_parsed_ent} entities in {round(time.time() - start ,3)} seconds from {n_files} files."
    )

    return entities


def get_url(model_type):
    if model_type.lower() == "dp":
        url = "http://209.182.238.238:5001/model"
    elif model_type.lower() == "sp":
        url = "http://209.182.238.238:7677/ner"
    return url

def get_word_and_entity(word_dict):
    word = list(word_dict)[0]
    entity = word_dict[word]
    return word, entity


def return_next_entity_index(n, list_):
    next_word_dict = list_[n + 1]
    _, next_entity = get_word_and_entity(next_word_dict)
    if next_entity[0] == "I":
        return "I"
    elif next_entity[0] == "B":
        return "B"


def get_nice_entities(preds_list):
    """Return list of entities from list of dictionaries like:
     [{'Tel': 'B-ORG'}, {'aviv': 'I-ORG'}, {'yafoo': 'B-GPE'}, {'moscow': 'B-GPE'}]"""
    entities = []
    l = len(preds_list)
    for n, word_dict in enumerate(preds_list):
        word, entity = get_word_and_entity(word_dict)
        if entity == "O":
            continue

        # if n < (l - 1):
        while n < (l - 1) and return_next_entity_index(n, preds_list) == "I":
            n = n + 1
            word_new, entity_new = get_word_and_entity(preds_list[n])
            word = word + " " + word_new
            preds_list[n][word_new] = "skip"

        if entity == "skip":
            continue

        #entity = entity.split("-")[1]
        word = clean_punctuation(word)

        entities.append(word)

    return entities


def get_entities_from_DP(resp_dict):
    entities_tmp = []
    for word, ent in zip(resp_dict[0][0], resp_dict[0][1]):
        if ent!=0:
            entities_tmp.append({word:ent})

    entities = get_nice_entities(entities_tmp)
    return entities



def get_ners(model_type, text):
    #start = time.time()
    if model_type.lower() == "dp":
        payload = json.dumps({"x": [text]})
    elif model_type.lower() == "sp":
        payload = json.dumps({"text": text})
    headers = {"Content-Type": "application/json"}
    url = get_url(model_type)
    response = requests.post(url=url, data=payload, headers=headers)
    entities = []
    if int(response.status_code) != 200:
        print(f"Response is not working! CHECK IT!")
    else:  # code 200 means we get correct response from the service
        resp_dict = response.json()
        if model_type.lower() == "dp":
            entities = get_entities_from_DP(resp_dict)
        elif model_type.lower() == "sp":
            if len(resp_dict["response"]) > 0:  # check if there is more than 0 entries
                resp_ent_l = resp_dict["response"]["entities"]
                for el in resp_ent_l:
                    entities.append(clean_punctuation(el["value"]))

    # print(f"Extracted entities: {entities} for {round(time.time() - start, 3)} seconds")

    return entities


def extract_all_entities_by_index(model_type, entities, index):
    all_ents = []
    for n in entities:
        if n == "filenames":
            continue
        elif n == "titles":
            if len(entities[n][index]) > 0:
                new_entities = get_ners(model_type, entities[n][index][0])
                all_ents.extend(new_entities)
        else:
            all_ents.extend(entities[n][index])

    return all_ents


def beautify_filename(filename):
    return "/" + filename.split(".")[0]


def write_entities(model_type, entities, filename):
    start = time.time()
    print("-" * 33)
    with open(filename + ".csv", "w") as f:
        file = csv.writer(open(filename + ".csv", "w"))
        for i in tqdm.tqdm(range(len(entities["filenames"])), desc="Records"):
            all_ent_by_record = extract_all_entities_by_index(model_type, entities, i)
            all_ent_by_record = [clean_punctuation(i) for i in all_ent_by_record]
            for n in range(len(all_ent_by_record)):
                entity_id = f"{str(n)}_{beautify_filename(entities['filenames'][i])}"
                row_to_write = [f"{all_ent_by_record[n]}", f"{entity_id}"]
                file.writerow(row_to_write)
    print(
        f"Entities written to file: {filename+'.csv'} for {round(time.time()-start , 3)} seconds"
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Indicate which zip file to parse")
    parser.add_argument(
        "--model", type=str, help="indicate which model to use: `DP`(DeepPavlov) or `SP` (SimpleTransfomers) ",
    )
    parser.add_argument(
        "--zipname", type=str, help="indicate full filename of zip to parse",
    )
    parser.add_argument(
        "--filename",
        type=str,
        default="false",
        help="indicate name of file to write entities without extension",
    )
    parser.add_argument(
        "--n",
        type=str,
        default=0,
        help="indicate number of entities needed to be parsed",
    )
    args = parser.parse_args()
    model_type = args.model.lower()
    zipname = args.zipname
    filename = args.filename
    n_need_ent = int(args.n)


    entities = extract_entities_from_zip(zipname, n_need_ent)
    write_entities(model_type, entities, filename)

