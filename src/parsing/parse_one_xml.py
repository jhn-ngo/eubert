import time
import random
import argparse
from lxml import etree  # need this to support `tag`


XML_LANG = "{http://www.w3.org/XML/1998/namespace}lang"

tag_prefLabel = "{http://www.w3.org/2004/02/skos/core#}prefLabel"
tag_altLabel = "{http://www.w3.org/2004/02/skos/core#}altLabel"

EUROPEANA_PROXY = "{http://www.europeana.eu/schemas/edm/}europeanaProxy"
tag_dc_contributor = "{http://purl.org/dc/elements/1.1/}contributor"
tag_dc_spatial = "{http://purl.org/dc/terms/}spatial"
tag_dc_issued = "{http://purl.org/dc/terms/}issued"
tag_dc_type = "{http://purl.org/dc/elements/1.1/}type"
tag_dc_title = "{http://purl.org/dc/elements/1.1/}title"
tag_dc_subject = "{http://purl.org/dc/elements/1.1/}subject"

flatten = lambda l: [item for sublist in l for item in sublist]


def randomly_select_entry(list_of_entries):
    """Function randomly select entry which is not in language which is forbidden.
    It return dictionary with format {lang:value}, where lang and value are strings.
    If there is no such entries it returns None.

    List of forbidden languages:
    'ko': '장 콕토'
    'zh': '让·谷克多'
    'ja': 'ジャン・コクトー'
    'ka': 'ჟან კოქტო'
    'hy': 'Ժան Կոկտո'
    'ar': 'جان كوكتو'
    """

    if len(list_of_entries) == 0:
        return None

    languages_not_use = ["ko", "zh", "ja", "ka", "hy", "ar"]

    lang_val_dict = random.choice(list_of_entries)
    if list(lang_val_dict)[0] in languages_not_use:
        while list(lang_val_dict)[0] in languages_not_use:
            list_of_entries.remove(lang_val_dict)
            lang_val_dict = random.choice(list_of_entries)

    entity = lang_val_dict[list(lang_val_dict)[0]]

    return entity


def get_namespaces_from_xml(filename):
    """Function return tuple containing NS(EDM:AGENT), NS(SKOS:CONCEPT) and (ORE:PROXY)
    If there is no such namespace in xml file it returns empty string in corresponding position."""

    tree = etree.parse(filename)
    root = tree.getroot()
    children_tags = list(set([child.tag for child in root]))
    ns_edm_agent, ns_skos_concept, ns_ore_proxy = "", "", ""
    for child_tag in children_tags:
        if "edm" and "Agent" in child_tag:
            ns_edm_agent = child_tag
        if "skos" and "Concept" in child_tag:
            ns_skos_concept = child_tag
        if "ore" and "Proxy" in child_tag:
            ns_ore_proxy = child_tag

    return (ns_edm_agent, ns_skos_concept, ns_ore_proxy)


def get_random_entity_from_segment_by_tag(segment, tag):
    labels = segment[1].findall(tag)
    entities = [{element.attrib[XML_LANG]: element.text} for element in labels]
    entity = randomly_select_entry(entities)
    return entity


def parse_ore(filename, ns_ore_proxy):
    persons_entity = list()
    locations_entity = list()
    dates_entity = list()
    concepts_entity = list()
    subject_entity = list()
    titles_encoding = list()

    context = iter(etree.iterparse(filename, events=("start",), tag=ns_ore_proxy))

    for n, segment in enumerate(context):
        is_europeana_proxy = [
            element.text for element in segment[1].findall(EUROPEANA_PROXY)
        ][0]
        if is_europeana_proxy == "true":  # check that it's no europeana proxy
            continue

        persons_entity.append([a.text for a in segment[1].findall(tag_dc_contributor)])
        locations_entity.append([a.text for a in segment[1].findall(tag_dc_spatial)])
        dates_entity.append([a.text for a in segment[1].findall(tag_dc_issued)])
        concepts_entity.append([a.text for a in segment[1].findall(tag_dc_type)])
        subject_entity.append([a.text for a in segment[1].findall(tag_dc_subject)])
        titles_encoding.append([a.text for a in segment[1].findall(tag_dc_title)])

    return (
        flatten(persons_entity),
        flatten(locations_entity),
        flatten(dates_entity),
        flatten(concepts_entity),
        flatten(subject_entity),
        flatten(titles_encoding),
    )


def parse_skos_concept(filename, ns_skos_concept):
    concepts = list()
    context = iter(etree.iterparse(filename, events=("start",), tag=ns_skos_concept))

    for n, segment in enumerate(context):
        concepts.append(get_random_entity_from_segment_by_tag(segment, tag_prefLabel))

    return concepts


def parse_edm_agent(filename, ns_edm_agent):
    context = etree.iterparse(filename, events=("start",), tag=ns_edm_agent)
    persons = list()
    for n, segment in enumerate(context):
        pref_entity = get_random_entity_from_segment_by_tag(segment, tag_prefLabel)
        persons.append(pref_entity)
        alt_entity = get_random_entity_from_segment_by_tag(segment, tag_altLabel)
        persons.append(alt_entity)
    persons = [i for i in persons if i != None]
    return persons


def parse_one_xml(filename):
    start = time.time()
    ns_edm_agent, ns_skos_concept, ns_ore_proxy = get_namespaces_from_xml(filename)

    if ns_edm_agent != "":
        edm_persons = parse_edm_agent(filename, ns_edm_agent)
    else:
        edm_persons = []

    if ns_ore_proxy != "":
        (
            ore_persons,
            ore_locations,
            ore_dates,
            ore_concepts,
            ore_subjects,
            ore_titles,
        ) = parse_ore(filename, ns_ore_proxy)
    else:
        (
            ore_persons,
            ore_locations,
            ore_dates,
            ore_concepts,
            ore_subjects,
            ore_titles,
        ) = ([], [], [], [], [], [])

    if ns_skos_concept != "":
        skos_concepts = parse_skos_concept(filename, ns_skos_concept)
    else:
        skos_concepts = []

    persons = list(set(edm_persons + ore_persons))
    places = list(set(ore_locations))
    concepts = list(set(ore_concepts + skos_concepts + ore_subjects))
    dates = list(set(ore_dates))
    titles = list(set(ore_titles))

    n_ent = len(persons) + len(places) + len(concepts) + len(dates) + len(titles)
    end = time.time()

    print()
    print(
        f"File: {filename} parsed for {round((end-start), 3)} seconds. It contains {n_ent} entites."
    )
    print("-" * 33)
    print("PERSONS: ", persons)
    print("PLACES: ", places)
    print("CONCEPTS: ", concepts)
    print("DATES: ", dates)
    print("TITLES: ", titles)

    return (filename, n_ent, persons, places, concepts, dates, titles)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Indicate which file to parse")
    parser.add_argument(
        "--filename",
        type=str,
        help="indicate full filename to parse",
    )
    args = parser.parse_args()
    filename = args.filename
    parse_one_xml(filename)






