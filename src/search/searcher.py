def edit_entities_filename_for_europeana(filename_str):
    """Function transform entities.csv key to one which could be sent to europeana.
    For example string `0_/04802/1AAAE9A1B793D4271C47196C60C73B72CE30E620` will be transformed to:
    `/04802/1AAAE9A1B793D4271C47196C60C73B72CE30E620'`"""
    filename_l = filename_str.split("/")
    filename_str = "/" + "/".join(filename_l[1:])
    return filename_str


def form_query_string(keys):
    """Function form query string to europeana search API"""

    query_string = "(europeana_id:"
    keys_l = len(keys)

    for i in range(keys_l):
        if i == keys_l - 1:  # last element in the list
            query_string += f'"{keys[i]}")'
        else:
            query_string += f'"{keys[i]}" OR europeana_id:'

    return query_string


def merge_results(first_dict, second_dict):
    """Function merge results from different queries into one dictionary with updated items"""

    merged_dict = first_dict.copy()  # in order not refer to initial object
    for item in second_dict["items"]:
        merged_dict["items"].append(item)
    merged_dict["itemsCount"] = first_dict["itemsCount"] + second_dict["itemsCount"]
    merged_dict["totalResults"] = (
        first_dict["totalResults"] + second_dict["totalResults"]
    )

    return merged_dict


def get_ners(text):
    """Function return list of NER's (without indication type of ner) from text"""

    url = "http://209.182.238.238:7678/ner"  # url of docker with running model
    payload = json.dumps({"text": text})  # creating request with input text
    headers = {"Content-Type": "application/json"}  # indicating headers
    response = requests.post(url=url, data=payload, headers=headers)
    resp_dict = response.json()  # getting response in json format
    entities_l = resp_dict["response"][
        "entities"
    ]  # extracting list with dictionaries of entities
    entities = [i["value"] for i in entities_l]

    return entities


def get_records_keys_from_ners(source_query):
    """Function returns keys (europeana_id) of records which were founded by closest vectors to NER's ,
    extracted from input query

     - In case there is no ners just encode vector - return empty list
     - In case there is one ner - encode it and work with it
     - In case there is a lot of NER's encode them all and merge all the results

    """

    keys = []
    source_query_text = ",".join(source_query)  # source was list of text
    ners = get_ners(
        source_query_text
    )  # translate list into text # expect it returns list of ners
    logging.info(f"NER's extracted from source query: {ners}")
    if len(ners) == 0:
        #    logging.info(f"Encoding vector: {source_query_text} ...")
        #    vectors = encoder.encode(source_query)
        #    for vector in vectors:
        #        results = engine.search(vector.tolist())
        #        for result in results:
        #            key = titles[result.id][0]
        #            keys.append(key)
        keys = []
    else:
        for n, ner in enumerate(ners):
            logging.info(f"Encoding vector: {ner} ({n}) ...")
            vectors = encoder.encode([ner])
            logging.info(f"Searching in milvus for vectors: {vectors}")
            for vector in vectors:
                logging.info(f"Search in milvus for vector: {vector}")
                results = engine.search(vector.tolist())
                for result in results:
                    # print(result.id, " --> ", titles[result.id])
                    key = titles[result.id][0]
                    keys.append(key)

    return keys


def add_key(results):
    """Function return results with added key"""
    for n, item in enumerate(results["items"]):
        keys = [k for k in item.keys()]
        key = ",".join(item["title"]) + " "
        if "dcCreator" in keys:
            key += ",".join(item["dcCreator"]) + " "
        if "dcContributor" in keys:
            key += ",".join(item["dcContributor"]) + " "
        #if "dcDescription" in keys:
        #    key += ",".join(item["dcDescription"]) + " "

        results["items"][n].update({"key": [key]})

    return results


def get_top_n_results(results, n=5, last=False):
    n = min(len(results), n)
    if last:
        top_n_items = results["items"][-n:]
    else:
        top_n_items = results["items"][:n]
    titles = [k["title"] for k in top_n_items]
    descriptions = [
        k["dcDescription"][0][:200] if "dcDescription" in [l for l in k.keys()] else ""
        for k in top_n_items
    ]
    data_providers = [k["dataProvider"] for k in top_n_items]
    links = [k["edmPreview"] for k in top_n_items]

    results_final = [
        {
            "title": titles[k],
            "dataprovider": data_providers[k],
            "description": descriptions[k],
            "link": links[k],
        }
        for k in range(len(top_n_items))
    ]

    return results_final


def print_results(results, color="green", last=False):
    if last:
        results = get_top_n_results(results, last=True)
    else:
        results = get_top_n_results(results)
    for n, res in enumerate(results):
        cprint(f"{n}: title: {res['title']}", color)
        cprint(f"description: {res['description']}", color)
        cprint(f"link: {res['link']}", color)


def get_europeana_results(source_query):
    """Function return results obtained directly from europeana"""

    # TO DO: Should we add filter by collection here as well? (theme: "ww1")
    source_query_text = ",".join(source_query)  # source was list of text
    eu_endpoint = "https://www.europeana.eu/api/v2"
    eu_args = {"wskey": "UpLZkGEc2", "query": source_query_text, "theme":"ww1"}
    logging.info(
        f"Getting results directly from europeana for query: {source_query_text}"
    )
    results_europeana = requests.get(
        "{}/search.json".format(eu_endpoint), params=eu_args
    ).json()

    return results_europeana


def print_final_results(results_europeana, results_pipeline):
    """Function simply prints in different colors results obtained directly from the europeana and from pipeline"""

    cprint("-" * 33, "yellow")
    cprint("EUROPEANA RESULTS", "yellow")
    print_results(results_europeana, "yellow")
    cprint("-" * 33, "green")
    cprint("PIPELINE RESULTS", "green")
    print_results(results_pipeline, "green")


def baseline_1(source_query):
    """Implementing simple pipeline of getting results from europeana and sending them to nboost.
    Function return europeana results and results of the whole pipeline"""

    logging.info("Starting Baseline 1 . . . ")
    source_query_text = ",".join(source_query)  # source was list of text
    results_europeana = get_europeana_results(source_query)
    logging.info(f"Query string to nboost: {source_query_text}")
    eu_endpoint = "http://localhost:8000"
    eu_args = {"wskey": "UpLZkGEc2", "query": source_query_text}
    logging.info("Getting results from nboost ...")
    results_nboost = requests.get(
        "{}/search".format(eu_endpoint), params=eu_args
    ).json()

    return results_europeana, results_nboost


def baseline_2(source_query):
    """Implementing pipeline at which we unite results with milvus only in case there is at least one NER"""

    logging.info("Starting Baseline 2 . . . ")
    source_query_text = ",".join(source_query)  # source was list of text
    keys = get_records_keys_from_ners(source_query)
    if len(keys) == 0:
        logging.info(
            f"NER's wasn't founded in {source_query_text}. Results will be same as in the first baseline!"
        )
        results_europeana, results_nboost = baseline_1(source_query)
    else:
        logging.info(f"NER's were founded in {source_query_text}.")
        # sending request to europeana API to get records by keys
        query_string = form_query_string(keys)
        logging.info(f"Query string by ners: {query_string}")
        #   eu_endpoint = "http://localhost:5757" # euproxy running port # doesn't work!
        eu_endpoint = "https://www.europeana.eu/api/v2"
        eu_args = {"wskey": "UpLZkGEc2", "query": query_string}
        logging.info("Getting results from milvus ...")
        results_milvus = requests.get(
            "{}/search.json".format(eu_endpoint), params=eu_args
        ).json()
        cprint("-" * 33, "magenta")
        cprint("RESULTS FROM MILVUS", "magenta")
        print_results(results_milvus, "magenta")

        # getting results directly from europeana API
        results_europeana = get_europeana_results(source_query)

        # merge results:
        results_merged = merge_results(results_milvus, results_europeana)
        logging.info("Adding keys to merged results . . .")
        results_merged = add_key(results_merged)

        # push results to nboost for final rearrange
        keys = [k["id"] for k in results_merged["items"]]
        query_string = form_query_string(keys)
        logging.info(f"Query string to nboost: {query_string}")
        eu_endpoint = "http://localhost:8000"
        eu_args = {
            "wskey": "UpLZkGEc2",
            "query": query_string,
            "source_query": source_query_text,
        }
        logging.info("Getting results from nboost ...")
        results_nboost = requests.get(
            "{}/search".format(eu_endpoint), params=eu_args
        ).json()

    return results_europeana, results_nboost


def baseline_3(source_query):
    """Implementing pipeline at which we unite results with milvus encoding query even there is no NER inside"""

    logging.info("Starting Baseline 3 . . . ")
    source_query_text = ",".join(source_query)  # source was list of text
    keys = get_records_keys_from_ners(source_query)
    if len(keys) == 0:
        logging.info(f"NER's wasn't founded in {source_query_text}.")
        logging.info(f"Encoding vector: {source_query_text} ...")
        vectors = encoder.encode(source_query)
        logging.info(f"Finding nearest in milvus for each vector")
        for vector in vectors:
            logging.info(f"Finding nearest in milvus for vector: {vector}")
            results = engine.search(vector.tolist())
            for result in results:
                key = titles[result.id][0]
                keys.append(key)

        query_string = form_query_string(keys)
        logging.info(f"Query string by source query encoding: {query_string}")
#        eu_endpoint = "http://localhost:5757" # euproxy running port # doesn't work!
        eu_endpoint = "https://www.europeana.eu/api/v2"
        eu_args = {"wskey": "UpLZkGEc2", "query": query_string}
        logging.info("Getting results from milvus ...")
        results_milvus = requests.get(
            "{}/search.json".format(eu_endpoint), params=eu_args
        ).json()
        cprint("-" * 33, "magenta")
        cprint("MILVUS RESULTS", "magenta")
        print_results(results_milvus, "magenta")

        # getting results directly from europeana API
        results_europeana = get_europeana_results(source_query)

        # merge results:
        results_merged = merge_results(results_milvus, results_europeana)
        logging.info("adding keys to merged results . . .")
        results_merged = add_key(results_merged)

        # push results to nboost for final rearrange
        keys = [k["id"] for k in results_merged["items"]]
        query_string = form_query_string(keys)
        print(f"Query string to nboost: {query_string}")
        eu_endpoint = "http://localhost:8000"
        start = time.time()
#        eu_endpoint = "http://35.223.246.226:8000"
        eu_args = {
            "wskey": "UpLZkGEc2",
            "query": query_string,
            "source_query": source_query_text,
        }
        logging.info(f"Getting results from nboost endpoint {eu_endpoint} ...")
        results_nboost = requests.get(
            "{}/search".format(eu_endpoint), params=eu_args
        ).json()
        logging.info(f"Received results from nboost in: {round(time.time()-start, 3)} s")
    else:
        logging.info(
            f"NER's were founded in {source_query_text}. Results will be same as in the second baseline!"
        )
        logging.info(f"NER's were founded in {source_query_text}.")
        # sending request to europeana API to get records by keys
        query_string = form_query_string(keys)
        logging.info(f"Query string by ners: {query_string}")
        #   eu_endpoint = "http://localhost:5757" # euproxy running port # doesn't work!
        eu_endpoint = "https://www.europeana.eu/api/v2"
        eu_args = {"wskey": "UpLZkGEc2", "query": query_string}
        logging.info("Getting results from milvus ...")
        results_milvus = requests.get(
            "{}/search.json".format(eu_endpoint), params=eu_args
        ).json()
        cprint("-" * 33, "magenta")
        cprint("RESULTS FROM MILVUS", "magenta")
        print_results(results_milvus, "magenta")

        # getting results directly from europeana API
        results_europeana = get_europeana_results(source_query)

        # merge results:
        results_merged = merge_results(results_milvus, results_europeana)
        logging.info("Adding keys to merged results . . .")
        results_merged = add_key(results_merged)

        # push results to nboost for final rearrange
        keys = [k["id"] for k in results_merged["items"]]
        query_string = form_query_string(keys)
        logging.info(f"Query string to nboost: {query_string}")
        start = time.time()
        eu_endpoint = "http://localhost:8000"
#        eu_endpoint = "http://35.223.246.226:8000"
        eu_args = {
            "wskey": "UpLZkGEc2",
            "query": query_string,
            "source_query": source_query_text,
        }
        logging.info(f"Getting results from nboost endpoint: {eu_endpoint} ...")
        results_nboost = requests.get(
            "{}/search".format(eu_endpoint), params=eu_args
        ).json()
        logging.info(f"Received results from nboost in: {round(time.time()-start, 3)} s")

    return results_europeana, results_nboost


if __name__ == "__main__":
    import logging, time
    import sys, csv
    import json, requests

    sys.path.append("/opt/eubert/src/")  # fix paths in whole module later
    from search.engine import SearchEngine
    from encoder.stencoder import STEncoder
    from termcolor import cprint

    logging.basicConfig(
        format="%(asctime)s:%(levelname)s:%(message)s", level=logging.DEBUG
    )

    csv_filename = sys.argv[2]
    if "titles" in csv_filename:
        engine = SearchEngine()
    elif "entities" in csv_filename:
        print("Loading milvus table:  entities")
        engine = SearchEngine("entities")
    logging.info("Loading encoder ...")
    encoder = STEncoder(sys.argv[1])

    logging.info(f"Loading {csv_filename} ...")
    with open(csv_filename, "r") as kf:
        fcsv = csv.reader(kf)
        if "titles" in csv_filename:
            titles = [k for k in fcsv]
        elif "entities" in csv_filename:
            titles = [[edit_entities_filename_for_europeana(k[1]), k[0]] for k in fcsv]

    logging.info(f"First sample of {csv_filename}: {titles[:5]}")

    source_query = sys.argv[3:]
    source_query_text = ",".join(source_query)  # source was list of text

    # results_europeana, results_pipeline = baseline_1(source_query)
    # results_europeana, results_pipeline = baseline_2(source_query)
    results_europeana, results_pipeline = baseline_3(source_query)
    print_final_results(results_europeana, results_pipeline)
