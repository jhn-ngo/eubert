""" Flask proxy launcher script

This script launch flask app in order to proxy requests to europeana.
It accepts command line arguments in following and special format:

`-port` - to indicate port on which it will be launched

and any other parameters which starts with `-` or `--`.
For instance command line could look like:
`python euproxy.py -port 9000 -theme ww1`
and it will parse theme=ww1 automatically.

This script requires that `flask`, `flask_cors` and `werkzeug` be installed within the Python
environment you are running this script in.

This file cannot be imported as a module.
"""

import sys
import argparse
import logging
import requests
from flask import Flask, request, jsonify
from flask_cors import CORS
from werkzeug import ImmutableMultiDict


def parse_arguments():
    """Function parse command line arguments and returns dictionary of parsed values.
   Arbitary values should start with `-` or `--`
   """

    parsed_dict = {}
    parser = argparse.ArgumentParser()
    parser.add_argument("-port", type=int)
    parsed, unknown = parser.parse_known_args()  # this is an 'internal' method
    # which returns 'parsed', the same as what parse_args() would return
    # and 'unknown', the remainder of that
    # the difference to parse_args() is that it does not exit when it finds redundant arguments
    for arg in unknown:
        if arg.startswith(("-", "--")):
            parser.add_argument(arg)

    args = parser.parse_args()
    for arg in vars(args):
        parsed_dict[arg] = getattr(args, arg)

    return parsed_dict


parsed_dict = parse_arguments()
app = Flask(__name__)
CORS(app)


def query_europeana(eu_args):
    eu_endpoint = "https://www.europeana.eu/api/v2"
    eu_args = eu_args.copy()  # to add elements into immutabledict
    keys = [k for k in eu_args.keys()]
    if "source_query" in keys:
        eu_args.pop("source_query")  # if exists delete it
    for key in parsed_dict:
        eu_args.add(key, parsed_dict[key])
    # eu_args.add("theme", "ww1")  # adding filter by collection
    eu_args = ImmutableMultiDict(eu_args)  # returning dictionary to immutable
    results = requests.get("{}/search.json".format(eu_endpoint), params=eu_args).json()
    return results


@app.route("/search")
def search():
    # return jsonify({'result': 'Done'})
    return query_europeana(request.args)


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s:%(levelname)s:%(message)s", level=logging.DEBUG
    )
    port = parsed_dict["port"]
    app.run("0.0.0.0", port=port)
