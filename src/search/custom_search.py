import logging
import sys, csv
import json
import requests
import europeana

sys.path.append("/opt/eubert/src/")  # fix paths in whole module later
from search.engine import SearchEngine
from encoder.stencoder import STEncoder


def edit_entities_filename_for_europeana(filename_str):
    """Function transform entities.csv key to one which could be sent to europeana.
    For example string `0_/04802/1AAAE9A1B793D4271C47196C60C73B72CE30E620` will be transformed to:
    `/04802/1AAAE9A1B793D4271C47196C60C73B72CE30E620'`"""

    filename_l = filename_str.split("/")
    filename_str = "/" + "/".join(filename_l[1:])
    return filename_str


def read_csv(csv_filename):
    with open(csv_filename, "r") as kf:
        fcsv = csv.reader(kf)
        if "titles" in csv_filename:
            titles = [k for k in fcsv]
        elif "entities" in csv_filename:
            titles = [[edit_entities_filename_for_europeana(k[1]), k[0]] for k in fcsv]

    return titles


class SearchAPI:
    def __init__(
        self,
        titles: list,  # list of titles/entities and key from csv file
        encoder_model_path: str,
        engine_table_name: str,
        wskey: str,
    ):

        self.titles = titles
        self.encoder = STEncoder(encoder_model_path)  #  indicate modelpath to encoder
        self.engine = SearchEngine(engine_table_name)  # indicate table name here
        self.nboost_url = "http://35.223.246.226:8000"
        self.ner_url = "http://localhost:7678"  # 'http://209.182.238.238:7678/ner'
        self.headers = {"Content-Type": "application/json"}  # indicating headers
        self.wskey = wskey
        self.milvus_threshold = 20

    def get_ners(self, text):
        """Function return list of NER's (without indication type of ner) from text"""
        logging.debug("Get NERs")
        payload = json.dumps({"text": text})  # creating request with input text
        url = f"{self.ner_url}/ner"
        response = requests.post(url=url, data=payload, headers=self.headers).json()
        entities = [i["value"] for i in response["response"]["entities"]]

        logging.debug("Done - Get NERs")
        return entities

    def get_records_keys_from_ners(self, query):
        """Function returns keys (europeana_id) of records which were founded by closest vectors to NER's ,
        extracted from input query

         - In case there is no ners just encode vector - return empty list
         - In case there is one ner - encode it and work with it
         - In case there is a lot of NER's encode them all and merge all the results

        """

        keys = []
        ners = self.get_ners(
            query
        )  # translate list into text # expect it returns list of ners
        if len(ners) == 0:
            keys = []
        else:
            for n, ner in enumerate(ners):
                vectors = self.encoder.encode([ner])
                for vector in vectors:
                    logging.debug(
                        "Milvus search - {}, vector: {}".format(ner, vector.tolist())
                    )
                    results = self.engine.search(vector.tolist())
                    logging.debug("Done - Milvus search - {}".format(ner))
                    for result in results:
                        if (
                            result.distance > self.milvus_threshold
                        ):  # skip vectors which are too far in vector space
                            continue
                        # print(result.id, " --> ", titles[result.id])
                        key = self.titles[result.id][0]
                        keys.append(key)

        return keys

    def form_query_string(self, keys):
        """Function form query string to europeana search API"""

        query_string = "(europeana_id:"
        keys_l = len(keys)

        for i in range(keys_l):
            if i == keys_l - 1:  # last element in the list
                query_string += f'"{keys[i]}")'
            else:
                query_string += f'"{keys[i]}" OR europeana_id:'

        return query_string

    def merge_results(self, first_dict, second_dict):
        """Function merge results from different queries into one dictionary with updated items"""

        merged_dict = first_dict.copy()  # in order not refer to initial object
        if not "items" in merged_dict:
             merged_dict = {"items" : [], "itemsCount" : 0, "totalResults": 0}
        for item in second_dict["items"]:
            merged_dict["items"].append(item)
        merged_dict["itemsCount"] = first_dict.get("itemsCount", 0) + second_dict.get("itemsCount", 0)
        merged_dict["totalResults"] = (
            first_dict.get("totalResults", 0) + second_dict.get("totalResults", 0)
        )

        return merged_dict

    def add_key(self, results):
        """Function return results with added key"""
        for n, item in enumerate(results["items"]):
            keys = [k for k in item.keys()]
            key = ",".join(item["title"]) + " "
            if "dcCreator" in keys:
                key += ",".join(item["dcCreator"]) + " "
            if "dcContributor" in keys:
                key += ",".join(item["dcContributor"]) + " "
            # if "dcDescription" in keys:
            #    key += ",".join(item["dcDescription"]) + " "

            results["items"][n].update({"key": [key]})

        return results

    def api_search_baseline_1(self, query: str = "asiago", rows=100, start=1) -> dict:
        """Implementing simple pipeline of getting results from europeana and sending them to nboost.
        Function return europeana results and results of the whole pipeline"""
        logging.debug("Starting baseline 1")
        params = {"wskey": self.wskey, "query": query}
        url = f"{self.nboost_url}/search"
        logging.debug("Reranking via NBoost")
        response: dict = requests.get(url, params).json()
        logging.debug("Done")

        return response

    def api_search_baseline_2(self, query: str = "asiago", rows=100, start=1) -> dict:
        """Implementing pipeline at which we unite results with milvus only in case there is at least one NER"""

        logging.debug("Starting baseline 2")
        keys = self.get_records_keys_from_ners(query)
        if len(keys) == 0:
            response = self.api_search_baseline_1(query, rows, start)
        else:
            query_string = self.form_query_string(keys)
            logging.debug("Searching Milvus")
            milvus_response = europeana.EuropeanaAPI.api_search_json(
                europeana.EuropeanaAPI(self.wskey),
                query=query_string,
                rows=rows,
                start=start,
            )
            logging.debug("Searching Europeana")
            europeana_response = europeana.EuropeanaAPI.api_search_json(
                europeana.EuropeanaAPI(self.wskey), query=query, rows=rows, start=start
            )
            logging.debug("Merging results")
            results_merged = self.merge_results(milvus_response, europeana_response)
            results_merged = self.add_key(results_merged)

            keys = [k["id"] for k in results_merged["items"]]
            query_string = self.form_query_string(keys)

            params = {
                "wskey": self.wskey,
                "query": query_string,
                "source_query": query,
            }

            logging.debug("Reranking via NBoost")
            response = requests.get(
                "{}/search".format(self.nboost_url), params=params
            ).json()
            logging.debug("Done")

        return response

    def api_search_baseline_3(self, query: str = "asiago", rows=100, start=1) -> dict:
        """Implementing pipeline at which we unite results with milvus encoding query even there is no NER inside"""
        logging.debug("Starting baseline 3")
        logging.debug("Getting record keys from NERS")
        keys = self.get_records_keys_from_ners(query)
        if len(keys) == 0:
            logging.debug("Encoding keys")
            vectors = self.encoder.encode([query])
            for vector in vectors:
                results = self.engine.search(vector.tolist())
                for result in results:
                    if (
                        result.distance > self.milvus_threshold
                    ):  # skip vectors which are too far in vector space
                        continue
                    key = self.titles[result.id][0]
                    keys.append(key)

        logging.debug("Searching Milvus")
        query_string = self.form_query_string(keys)
        milvus_response = europeana.EuropeanaAPI.api_search_json(
            europeana.EuropeanaAPI(self.wskey),
            query=query_string,
            rows=rows,
            start=start,  # think about these params
        )
        logging.debug("Searching Europeana")
        europeana_response = europeana.EuropeanaAPI.api_search_json(
            europeana.EuropeanaAPI(self.wskey),
            query=query,
            rows=rows,
            start=start,  # check params here
        )
        logging.debug("Merging results")
        results_merged = self.merge_results(milvus_response, europeana_response)
        results_merged = self.add_key(results_merged)

        keys = [k["id"] for k in results_merged["items"]]
        query_string = self.form_query_string(keys)

        params = {
            "wskey": self.wskey,
            "query": query_string,
            "source_query": query,
        }

        logging.debug("Reranking via NBoost")
        response = requests.get(
            "{}/search".format(self.nboost_url), params=params
        ).json()
        logging.debug("Done")

        return response

    def api_search_json(self, query: str = "asiago", rows=100, start=1, pipeline=3):
        """Function runs corresponding baseline. By default it runs 3rd pipeline."""

        if pipeline == 1:
            response = self.api_search_baseline_1(query, rows, start)
        elif pipeline == 2:
            response = self.api_search_baseline_2(query, rows, start)
        else:  # by default running 3rd baseline
            response = self.api_search_baseline_3(query, rows, start)

        return response
