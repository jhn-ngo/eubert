from milvus import Milvus, IndexType, MetricType, Status

class SearchEngine:
   def __init__(self, table_name='titles_with_entities', host='localhost', port='19530'):
      self.milvus = Milvus()
      self.milvus.connect(host=host, port=port)
      self.table_name = table_name
      status,exists = self.milvus.has_table(table_name)
      if not exists:
          status = self.milvus.create_table({'table_name': table_name, 'dimension': 768})
          print(status)
          index_param = {'index_type': IndexType.IVFLAT, 'nlist': 16384}
          self.milvus.create_index(table_name, index_param)


   def index(self, vectors, keys):
      status, ids = self.milvus.add_vectors(table_name=self.table_name, records=vectors, ids=keys)
      print(status)
      return status

   def search(self, vector):
     status, results = self.milvus.search_vectors(table_name=self.table_name, query_records=[vector], top_k=10, nprobe=16)
     return results[0]

if __name__ == "__main__":
   import sys, numpy, tqdm
   if len(sys.argv) > 3:
      table_name = sys.argv[3]
      engine = SearchEngine(table_name)
   else:
      engine = SearchEngine()
   print(f"Table name: {engine.table_name}")
   fvectors = open(sys.argv[1], 'rb')   
   fkeys = open(sys.argv[2], 'r')   

   BATCH_SIZE = 500
   keys = []
   vectors = []
   i = 0
   for key in tqdm.tqdm(fkeys):
      vector = numpy.load(fvectors)
      vectors.append(vector.tolist())
      keys.append(i)
      i += 1
      if len(keys) >= BATCH_SIZE:
         engine.index(vectors,keys)
         keys = []
         vectors = []      
   if len(keys):
       engine.index(vectors,keys)
   fvectors.close()
   fkeys.close()
