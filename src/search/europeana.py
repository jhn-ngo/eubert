import requests


class EuropeanaAPI:
    def __init__(self, wskey):
        self.base_url = 'https://www.europeana.eu/api'
        self.version = 'v2'
        self.wskey = wskey

    def get_api_call(self, endpoint_name: str, params: dict) -> dict:
        """
        Base method. Make call for Europeana API.

        :param str endpoint_name: target endpoint for Europeana.
        :param dict params: contains param for request. Like 'query', 'row', etc.
        :return: API response itself.
        """
        params.update({'wskey': self.wskey})
        params.update({'media': 'true'})
        params.update({'theme': 'ww1'}) # adding filter by collection
        url = f'{self.base_url}/{self.version}/{endpoint_name}'
        response: dict = requests.get(url, params=params).json()

        return response

    def api_search_json(self, query='asiago', rows=100, start=1) -> dict:
        """
        :param str query: param of query for Europeana API.
        :param int rows: quantity of returns records.
        :return: response from Europeana.
        """
        endpoint_name = 'search.json'
        params = {
            'query': query,
            'rows': rows,
            'start': start
        }

        response: dict = self.get_api_call(endpoint_name, params)

        return response
