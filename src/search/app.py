import logging

logging.basicConfig(format="%(asctime)s:%(levelname)s:%(message)s", level=logging.DEBUG)

import configparser
import argparse
import json
import time
import os
from random import randint

from flask import Flask, request, render_template, send_from_directory
from flask_cors import CORS

from europeana import EuropeanaAPI
from custom_search import read_csv, SearchAPI

template_folder = os.getenv("ENV_NAME", "templates")
app = Flask(__name__, template_folder=template_folder, static_folder=f'{template_folder}/static')
CORS(app)
app.config["TESTING"] = False
app.config["DEBUG"] = False
app.config["ini_configs"] = configparser.ConfigParser()
app.config["ini_configs"].read("config.ini")
app.config["euro_client"] = EuropeanaAPI(
    app.config["ini_configs"]["EUROPEANA"]["wskey"]
)
global PIPELINE
PIPELINE = 3
custom_client = SearchAPI(
    titles=read_csv(app.config["ini_configs"]["EUBERT"]["csv_entities"]),
    encoder_model_path=app.config["ini_configs"]["EUBERT"]["encoder_path"],
    engine_table_name=app.config["ini_configs"]["EUBERT"]["milvus_table"],
    wskey=app.config["ini_configs"]["EUROPEANA"]["wskey"],
)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/<filename>")  # favicon.ico
def statics(filename):
    return send_from_directory(app.template_folder, filename)


def get_convert_result(raw_data: dict) -> dict:
    """
    Converts data to Elastic format.

    :param dict raw_data: data from Europeana API.
    :return: data at Elastic format.
    """
    data = {
        "responses": [
            {"hits": {"total": raw_data["totalResults"], "max_score": 0.0, "hits": []}}
        ]
    }
    for record in raw_data["items"]:
        data["responses"][0]["hits"]["max_score"] = (
            record["score"]
            if (data["responses"][0]["hits"]["max_score"] < record["score"])
            else data["responses"][0]["hits"]["max_score"]
        )
        data["responses"][0]["hits"]["hits"].append(
            {"_score": record.pop("score"), "_id": record.pop("id"), "_source": record}
        )

    return data


@app.route("/pipeline", methods=["POST", "GET"])
def pipeline():
    """ Set pipeline number if type of request is POST.
        If type of request is GET - shows used pipeline
    """
    global PIPELINE
    if request.method == "POST":
        usr_request = request.get_json()
        pipeline = usr_request["baseline"]
        PIPELINE = int(pipeline)

    result = {"Pipeline": PIPELINE}

    return json.dumps(result)


@app.route("/api_search_json/<switcher>/<garbage>", methods=["POST"])
def get_info_from_europeana_to_front(switcher, garbage):
    """
    Receives a request, redirects it to Europeana API.
    The processing result is sent to the user.

    :return: response to the user.
    """
    global PIPELINE
    result = {}
    try:
        user_query = request.get_json()
        if user_query is None:  # The payload is invalid JSON sent by WebApp
            user_query = json.loads(request.data.decode().split("\n")[1])

        query = user_query["query"]["bool"]["must"][0]["bool"]["must"][0]["bool"][
            "should"
        ][0]["multi_match"]["query"]
        query_size = user_query["size"]
        start_from = user_query["from"] if user_query["from"] else 1

        now_timestamp = time.time()
        if switcher == "europeana" or PIPELINE == 0:
            response_from_europeana: dict = app.config["euro_client"].api_search_json(
                query=query, rows=query_size, start=start_from
            )
            result.update(get_convert_result(response_from_europeana))
            # TMP: introduce delay for Europeana to make it comparable with Eubert
            time.sleep(randint(1,4))

        else:
            response_from_eubert: dict = custom_client.api_search_json(
                query=query, rows=query_size, start=start_from, pipeline=PIPELINE
            )
            result.update(get_convert_result(response_from_eubert))

        result["responses"][0].update(
            {"took": int((time.time() - now_timestamp) * 1000.0)}
        )

    except ValueError:
        result.update({"error": "You should provide parameter of query."})

    return result


if __name__ == "__main__":
#    from gevent import monkey
#    monkey.patch_all()
#    from gevent.pywsgi import WSGIServer


    parser = argparse.ArgumentParser(description="Indicate settings for app: ")
    parser.add_argument(
        "--port", type=int, default=5050, help="indicate port for application",
    )
    parser.add_argument(
        "--pipeline", type=int, default=None, help="inndicate pipeline",
    )
    args = parser.parse_args()
    port = args.port
    if args.pipeline != None:
       PIPELINE = args.pipeline

    app.run(host="0.0.0.0", port=port, debug=0)
    #server = WSGIServer(('0.0.0.0', port), app)
    #server.serve_forever()


