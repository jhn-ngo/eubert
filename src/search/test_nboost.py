import requests, time
import numpy as np

k = 100
start = time.time()
source_query_text = "asiago"
eu_endpoint = "http://localhost:8000"
eu_args = {"wskey": "UpLZkGEc2", "query": source_query_text}
times = []

for i in range(k):
    start_it = time.time()
    results_nboost = requests.get(f"{eu_endpoint}/search", params=eu_args).json()
    time_it = time.time()-start_it
    times.append(time_it)


times_np = np.array(times)

print(f"Mean time of iteration: {round(times_np.mean(), 5)}")
print(f"Std of time of iteration: {round(times_np.std(), 5)}")
print(f"Min time of iteration: {round(times_np.min(), 5)}")
print(f"Max time of iteration: {round(times_np.max(), 5)}")
print('-'*33)
print(f"{k} iterations of Nboost takes {round(time.time()-start, 5)} s")

