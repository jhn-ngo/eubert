from gevent import monkey
monkey.patch_all()

from app import app
from gevent.pywsgi import WSGIServer

server = WSGIServer(('0.0.0.0', 5050), app)
server.serve_forever()
